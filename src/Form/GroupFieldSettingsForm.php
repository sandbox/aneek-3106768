<?php

namespace Drupal\group_field\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Plugin\GroupContentEnablerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple configuration form to save the group field settings via group types.
 */
class GroupFieldSettingsForm extends ConfigFormBase {

  const CONFIG_NAME = 'group_field.field_settings';
  /**
   * The group content plugin manager.
   *
   * @var \Drupal\group\Plugin\GroupContentEnablerManagerInterface
   */
  protected $pluginManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $plugin_manager
   *   Group plugin manager interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, GroupContentEnablerManagerInterface $plugin_manager) {
    parent::__construct($config_factory);
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.group_content_enabler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getEditableConfigNames() {
    return [
      self::CONFIG_NAME
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'group_field_configuration';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupTypeInterface $group_type = NULL) {
    $form = [];
    $config = $this->config(self::CONFIG_NAME);
    // Check if the config is existing already.
    $thisSavedType = $config->get('entity_configuration.' . $group_type->id());
    $installed = $this->pluginManager->getInstalledIds($group_type);
    $plugins = [];
    foreach ($this->pluginManager->getAll() as $plugin_id => $plugin) {
      if (in_array($plugin_id, $installed)) {
        $pluginDetails = $group_type->getContentPlugin($plugin_id);
        $plugins[$plugin_id] = $pluginDetails->getLabel()->__toString();
      }
    }

    // Build the elements based on the plugins that are available.
    if (!empty($installed)) {
      $form['group_type'] = [
        '#type' => 'hidden',
        '#value' => $group_type->id()
      ];
      // Show the entities which are enabled.
      $form['enabled_entities'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Enable group field'),
        '#description' => $this->t('If checked, for these entities, the field will appear in the add/edit page of those entities.'),
        '#options' => $plugins,
        '#default_value' => (($thisSavedType === NULL) ? [] : $thisSavedType),
      ];

      $form += parent::buildForm($form, $form_state);
    }
    else {
      $this->messenger()->addError($this->t('No plugins are installed. Please install plugin and visit this page.'));
      $form += parent::buildForm($form, $form_state);
      unset($form['actions']);
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $enabledEntities = array_filter($values['enabled_entities']);
    // Keep in mind that we need to preserve the existing values.
    // So only alter the existing form's details and keep others as is.
    $existing = $this->config(self::CONFIG_NAME)
      ->get('entity_configuration');
    // If the same item is existing, then alter it.
    if (!empty($existing) && array_key_exists($values['group_type'], $existing)) {
      unset($existing[$values['group_type']]);
      if (!empty($enabledEntities)) {
        $existing[$values['group_type']] = array_values($enabledEntities);
      }
    }
    else {
      // This is new configuration?
      // Create an array of the elements to save in the configuration.
      if (!empty($enabledEntities)) {
        $existing[$values['group_type']] = array_values($enabledEntities);
      }
    }

    $this->config(self::CONFIG_NAME)
      ->set('entity_configuration', $existing)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
