<?php

namespace Drupal\group_field\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group\Plugin\GroupContentEnablerManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\group\Entity\Group;

/**
 * Defines a HTML select list field widget for the group selection.
 *
 * @FieldWidget(
 *   id = "group_field_group_select_widget",
 *   label = @Translation("Group Select Widget"),
 *   field_types = {
 *     "group_field_group_selector_field"
 *   },
 *   multiple_values = TRUE
 * )
 */
class GroupSelectWidget extends OptionsSelectWidget {

  /**
   * The list of options.
   *
   * @var array
   */
  protected $options;

  /**
   * Group Plugin manager instance.
   *
   * @var Drupal\group\Plugin\GroupContentEnablerManagerInterface
   */
  protected $groupPluginManager;

  /**
   * Drupal configuration factory interface.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class Constructor.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   Collection of plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   Field settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $group_content_enabler
   *   The group content enabler instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal configuration factory instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    GroupContentEnablerManagerInterface $group_content_enabler,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition,
      $settings, $third_party_settings);
    $this->groupPluginManager = $group_content_enabler;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.group_content_enabler'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    // Create a array of groups that can be assigned.
    if (!isset($this->options)) {
      // Get the entity type.
      $options = [];
      switch ($entity->getEntityTypeId()) {
        // @todo: Enable for other entities as well.
        case 'node':
          $entityType = $entity->bundle();
          $account = $entity->getOwner();
          $groupTypes = [];
          // What is the plugin id for the group?
          $groupPlugin = 'group_node:' . $entityType;
          // Load the configuration saved in the system and see if the current
          // entity bundle matches with it or not.
          $groupConfig = $this->configFactory
            ->get('group_field.field_settings')
            ->get('entity_configuration');
          if (!empty($groupConfig)) {
            foreach ($groupConfig as $groupType => $groupEntities) {
              // Check which group type matches the current entity bundle.
              if (in_array($groupPlugin, $groupEntities)) {
                $groupTypes[] = $groupType;
              }
            }
          }
          if (!empty($groupTypes)) {
            // We've got the group types and now load the groups for each type.
            $query = \Drupal::entityQuery('group');
            $query->condition('type', $groupTypes, 'IN');
            $groupIds = $query->execute();
            if (!empty($groupIds)) {
              $groups = Group::loadMultiple($groupIds);
            }
            foreach ($groups as $group) {
              $options[$group->id()] = $group->label();
            }
          }
          break;

        default:
          $options = parent::getOptions($entity);
          break;
      }
      $this->options = $options;
    }
    return $this->options;
  }

}
