<?php

namespace Drupal\group_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * A hidden formatter for the group selection field.
 *
 * @FieldFormatter(
 *   id = "group_field_group_hidden_formatter",
 *   label = @Translation("Group Field"),
 *   description = @Translation("This is a field formatter for the group selection."),
 *   module = "group_field",
 *   field_types = {
 *     "group_field_group_selector_field"
 *   }
 * )
 */
class GroupFieldHiddenFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // No need to return anything.
    return [];
  }

}
