<?php

namespace Drupal\group_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\group\Entity\Group;
use Drupal\node\NodeInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Group selection field.
 *
 * @FieldType(
 *   id = "group_field_group_selector_field",
 *   label = @Translation("Group Field"),
 *   module = "group_field",
 *   category = @Translation("Group"),
 *   description = @Translation("This creates a field to select group."),
 *   default_widget = "group_field_group_select_widget",
 *   default_formatter = "group_field_group_hidden_formatter",
 * )
 *
 * @package Drupal\group_field\Plugin\Field\FieldType
 */
class GroupSelectorField extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $properties = [];
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Group'));

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public function postSave($update) {
    $existingGroups = $new = $removed = [];
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    $pluginId = 'group_node:' . $entity->getType();
    $field = $this->getBundleFieldName($entity);
    if ($update) {
      // This is a node update.
      /** @var \Drupal\node\NodeInterface $original */
      $original = $entity->original;
      $existingGroups = $this->flatternGroupArray($original->get($field)->getValue());
    }
    $groups = $this->flatternGroupArray($entity->get($field)->getValue());
    // If we have the existing groups then check for duplicate and remove
    // from the actual array.
    if (!empty($existingGroups)) {
      $removed = array_diff($existingGroups, $groups);
    }
    // New groups are there?
    if (!empty($groups)) {
      foreach ($groups as $gid) {
        $groupEntity = Group::load($gid);
        $relation = $groupEntity->getContentByEntityId($pluginId, $entity->id());
        if (!$relation) {
          $groupEntity->addContent($entity, $pluginId);
        }
      }
    }
    // Anything Removed?
    if (!empty($removed)) {
      foreach ($removed as $gid) {
        $group_contents = \Drupal::entityTypeManager()
          ->getStorage('group_content')
          ->loadByProperties([
            'entity_id' => $entity->id(),
            'gid' => $gid,
          ]);
        foreach ($group_contents as $item) {
          $item->delete();
        }
      }
    }

  }

  /**
   * {@inheritDoc}
   */
  public function delete() {
    parent::delete();
  }

  /**
   * Method to get the bundle field name.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The current entity.
   *
   * @return string
   *   The name of the field.
   */
  private function getBundleFieldName(NodeInterface $entity) {
    $fieldName = '';
    // Dependency injection doesn't work for FieldType so use Containers.
    // @see: https://www.drupal.org/node/2053415
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($fields as $field => $config) {
      if ($config instanceof FieldConfigInterface &&
        $config->get('field_type') == 'group_field_group_selector_field'
      ) {
        $fieldName = $field;
        break;
      }
    }
    return $fieldName;
  }

  /**
   * Method removes 'value' key from the group array.
   *
   * @param array $groups
   *   The array containing the groups.
   *
   * @return array
   *   Returns either a blanl array or a full array containing the groups.
   */
  private function flatternGroupArray(array $groups = []) {
    $flattern = [];
    if (!empty($groups)) {
      foreach ($groups as $group) {
        $flattern[] = $group['value'];
      }
    }
    return $flattern;
  }

}
